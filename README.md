
<!-- README.md is generated from README.Rmd. Please edit that file -->

# bbmix pipeline

This pipeline is designed for running
[bbmix](https://gitlab.com/evigorito/bbmix) an R pacakge for calling
genotypes using RNA-seq

The pipeline includes code for input preparation starting from fastq
files with RNA-seq data

## Instalation

Clone the repository

``` bash
# clone the bbmix_pipeline repository
 git clone https://gitlab.com/evigorito/bbmix_pipeline.git
 
```

## System requirements

    R versions >= 3.4.0.
    GNU make
    bcftools
    samtools
    STAR
    Python v3 for allelic counts
           * numpy
           * scipy
           * pysam version 0.8.4 or higher
           * PyTables version 3.
    GATK
    tabix

  - To install STAR follow instructions in
    [STAR](https://github.com/alexdobin/STAR)
  - To install GATK follow
    [GATK](https://gatk.broadinstitute.org/hc/en-us/articles/360036194592-Getting-started-with-GATK4)
  - bcftools, htslib (contains tabix) and samtools can be downloaded
    from [htslib](http://www.htslib.org/download/)
  - To install Python3 and its libraries follow
    [python](https://realpython.com/installing-python/)

## External data

  - Reference panel We have used the [1000 Genomes
    Phase3](https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.html).
    The reference panel of choice should be in the hap/legend/sample
    format.

<!-- end list -->

``` bash
# Example files
wget https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.tgz
tar -zvxf 1000GP_Phase3.tgz
 
```

  - GRCh37 primary assembly &
GTF

<!-- end list -->

``` bash
wget ftp://ftp.ensembl.org/pub/grch37/current/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz
gunzip Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz
wget ftp://ftp.ensembl.org/pub/grch37/release-87/gtf/homo_sapiens/Homo_sapiens.GRCh37.87.gtf.gz
gunzip Homo_sapiens.GRCh37.87.gtf.gz
 
```

  - GRch37 reference fasta used in bcftools for RNA variant
callings

<!-- end list -->

``` bash
wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/human_g1k_v37.fasta.gz
 
```

### Working example

We provide a
[snakemake](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)
pipeline describing the steps of the ppeline. The working example
reproduces the input generation for the set of
[E-MTAB-1883](https://www.ebi.ac.uk/arrayexpress/experiments/E-MTAB-1883/)
at array express.

**You will find the following files and directories for running the
pipeline**

1.  The
    [Snakefile](https://gitlab.com/evigorito/bbmix_pipeline/-/blob/master/Snakefile)
    provides a user-friendly syntax. A brief description on how it is
    organized can be found below.

2.  The
    [config.yaml](https://gitlab.com/evigorito/bbmix_pipeline/-/blob/master/config.yaml)
    file list the paths to tools, external files (reference panel,
    genomic annotations, etc), and output directory to guide the
    Snakefile. This file needs to be edited with your own locations.

3.  The
    [Scripts](https://gitlab.com/evigorito/bbmix_pipeline/-/tree/master/Scripts)
    directory contains the R scripts that are called in the pipeline.

### Snakefile structure

1.  Set access to bashrc file and import relevant python modules
2.  Helper functions to run rules:
      - info\_sample: converts into a panda data frame the meta data
        from the study samples
      - dic\_samples: Makes a dictionary with keys sample names from
        E-MTAB-1883.sdrf.txt metadata and values any column from
        metadata.
3.  Rules for downloading study sample files
4.  Rules for applying GATK pipeline
5.  Rules for selecting variants to genotype
6.  Rules for preparing files for QC genotyping
7.  Rules for calling genotypes
