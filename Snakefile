
##################################################################
## Snakefile for input preparation and genotyping using BBmix 
#################################################################

shell.prefix("source ~/.bashrc; ")

configfile: "config.yaml"

localrules: all

import pandas as pd
import os

from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()

####################################################
## Based on samples from E-MTAB-1883 array express #
####################################################

def info_samples(s=config['bottle_dir'] + "/RNA/sample_info/E-MTAB-1883.sdrf.txt"):
    """
    Gets sample info including names and fastq paths from the meta data file in array express. 
    input: full file name
    output: data frame with sample meta data
    """
    data = pd.read_csv(s, sep="\t")
    return data

def dic_samples(var, s=config['bottle_dir'] + "/RNA/sample_info/E-MTAB-1883.sdrf.txt"):
    """ Get dict with keys sample names from E-MTAB-1883.sdrf.txt metadata and values any column from metadata.
    input: var is the variable name to use for dictionary values. Adapted from dic_samples function in psoriasis Snakefile"""
    df = info_samples(s)
    dic = {}
    keys= set(list(df['Source Name']))
    for k in keys:
        dic[k] = list(df.loc[df["Source Name"] == k, var])
    return dic

# Variables for workflow

sample_bottle = 'NA12878'
d=dic_samples(var='Comment[FASTQ_URI]')
chroms=[x+1 for x in range(22)]

# Choose option for model fitting (in input rule) for calling
# genotypes. When choosing "fit_model" in rule call_genotypes the file
# path for input argument beta_b will be used. If selecting "default"
# genotypes will be called with default model training.

model=["yes", "default"]


rule all:
    input:
        expand(config['output_dir'] + "/beta_binom_gt_all_fit_{model_fit}/chrom{chrom}.gt.txt",
               chrom=chroms, model_fit=model)
        # here you could replace model with model[0] or model[1]
        # depending of you choice for model training (explained above)

        
#######################################
## Download genome in a bottle sample
######################################

rule download_bottle:
    input:
        FTP.remote("ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release//NA12878_HG001/latest/README_NISTv3.3.2.txt", keep_local=True),FTP.remote(expand("ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release//NA12878_HG001/latest/GRCh37/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_{file}", file=["nosomaticdel.bed","PGandRTGphasetransfer.vcf.gz", "PGandRTGphasetransfer.vcf.gz.tbi"] ), keep_local=True)
    output:
        config['bottle_dir'] + "/GT/README_NISTv3.3.2.txt"
    run:
        shell("mv {input} {config[bottle_dir]}")

rule down_bottle_rna_metadata:
    """Download meta data for sample NA12878 which is the one genotyped in genome in a bottle. The RNA-seq data is in E-MTAB-1883 array express along with other samples"""
    input:
        HTTP.remote("www.ebi.ac.uk/arrayexpress/files/E-MTAB-1883/E-MTAB-1883.sdrf.txt", keep_local=True)
    output:
        config['bottle_dir'] + "/RNA/sample_info/E-MTAB-1883.sdrf.txt"
    run:
        outputName = os.path.join(config['bottle_dir'] + "/RNA/sample_info", os.path.basename(input[0]))
        shell("mv {input} {outputName} ")
        

rule fastq_down:
    """ Downloads fastq files """
    params:
        dirout=config['bottle_dir'] + "/RNA/RNAseq/"       
    output:
        out=expand(config['bottle_dir'] + "/RNA/RNAseq/{fastq}", fastq=[os.path.basename(f) for k in d.keys() for f in d[k]])  
    run:
        for k in d.keys():
            ftp= d[k]
            for f in ftp:           
                shell("cd {params.dirout} ; "
                      "wget {f} ")

# ######################
# Apply GATK pipeline  #
# #####################

rule STAR_1:
    """Align fastq files to genome using STAR with 2 passages. Start with first pass to collect SJ.out.tab files"""
    input:
        lambda wildcards: expand(config['bottle_dir'] + "/RNA/RNAseq/{fastq}", fastq=[os.path.basename(f) for f in d[wildcards.samp]])
    output:
        config['output_dir'] + "/STAR/built37/{samp}/Aligned.sortedByCoord.out.bam",
        config['output_dir'] + "/STAR/built37/{samp}/SJ.out.tab",
    params:
        index=config['indices'],
        read="zcat",
        out_dir=config['output_dir'] + "/STAR/built37/{samp}"
    threads: 16
    shell:
        "cd {params.out_dir}; "
        "{config[STAR]} "
        " --runThreadN {threads} "
        " --genomeDir {params.index} "
        " --readFilesIn {input} "
        " --readFilesCommand {params.read} "
        " --outSAMtype BAM SortedByCoordinate "
        " --outStd Log; "
        "samtools index {output[0]} "

rule STAR_2:
    """Align fastq files to genome using STAR with 2 passages. Second pass"""
    input:
        fq=lambda wildcards: expand(config['bottle_dir'] + "/RNA/RNAseq/{fastq}", fastq=[os.path.basename(f) for f in d[wildcards.samp]]),
        sj=expand(config['output_dir'] + "/STAR/built37/{samp}/SJ.out.tab",, samp=d.keys())
    output:
        config['output_dir'] + "/STAR/{samp}/Aligned.sortedByCoord.out.bam",
    params:
        index=config['indices'],
        read="zcat",
        out_dir=config['output_dir'] + "/STAR/{samp}/"
    threads: 16
    shell:
        "cd {params.out_dir}; "
        "{config[STAR]} "
        " --runThreadN {threads} "
        " --genomeDir {params.index} "
        " --readFilesIn {input.fq} "
        " --readFilesCommand {params.read} "
        " --outSAMtype BAM SortedByCoordinate "
        " --sjdbFileChrStartEnd {input.sj} "
        " --limitSjdbInsertNsj 2000000 "
        " --outStd Log; "
        "samtools index {output[0]} "

rule remove_dup:
    """This rule removes duplicated reads that map to the same location. I use the WASP implementation that removes reads at random and not the one with the higuest mapping score which will usually map the reference, to avoid ref panel bias"""
    input:
        config['output_dir'] + "/STAR/{samp}/Aligned.sortedByCoord.out.bam"
    output:
        rmdup=config['output_dir'] + "/rmdup/{samp}.rmdup.bam",
        sort=config['output_dir'] + "/rmdup/{samp}.rmdup.sort.bam"
    shell:
        "python {config[wasp]}/mapping/rmdup_pe.py {input} {output.rmdup} ;"
        "samtools sort -o {output.sort} {output.rmdup}; "
        "samtools index {output.sort}"


        
rule addGroup:
    """GATK pipeline requires to add groups even if one sample only, otherwise some tools reports errors dowsntream"""
    input:
        config['output_dir'] + "/rmdup/{samp}.rmdup.sort.bam"
    output:
        config['output_dir'] + "/addGroup/{samp}.rmdup.sort.bam"
    shell:
        "{config[gatk]} AddOrReplaceReadGroups "
        "-I {input} "
        "-O {output} "
        "-RGID 1 "
        "-RGLB lib1 "
        "-RGPL illumina "
        "-RGPU unit1 "
        "-RGSM {wildcards.samp}"

rule gatk_splitNcigar:
    """This rule splits reads that contain Ns in their cigar string (https://gatk.broadinstitute.org/hc/en-us/articles/360056970312-SplitNCigarReads). This step is recommended in the GATK best practices"""
    input:
        bam=config['output_dir'] + "/addGroup/{samp}.rmdup.sort.bam",
        fasta=config['ref_fasta']
    output:
        bam=config['output_dir'] + "/splitNcigar/{samp}.rmdup.sort.bam"
    shell:
        "{config[gatk]} SplitNCigarReads "
        "-R {input.fasta} "
        "-I {input.bam} "
        "-O {output.bam} "


rule down_variation:
    """For BQRC rule I need a file with all common variants. I downloaded this from GATK google cloud. To find the file path I used the gs//link (gs://gatk-legacy-bundles) in gatk website to then from google cloud get the https path"""
    input:
        HTTP.remote("https://storage.googleapis.com/gcp-public-data--broad-references/hg19/v0/Homo_sapiens_assembly19.dbsnp135.{vcf}", keep_local=True)
    output:
        config['ref_dir'] + "/gatk-legacy-bundles/Homo_sapiens_assembly19.dbsnp135.{vcf}"
    shell:
        "mv {input} {output} "
 
        
rule BQRC1:
    """Performs base quality recalibration after splitting cigar string in previous rule. Step1 is to prepare data table"""
    input:
        bam=config['output_dir'] + "/splitNcigar/{samp}.rmdup.sort.bam",
        ref_vcf=config['ref_dir'] + "/gatk-legacy-bundles/Homo_sapiens_assembly19.dbsnp135.vcf",
        fasta=config['ref_fasta']
    output:
        table=config['output_dir'] + "/BQRC/{samp}_data.table"
    shell:
        "{config[gatk]} BaseRecalibrator  "
        "-R {input.fasta} "
        "-I {input.bam} "
        "--known-sites {input.ref_vcf} "
        "-O {output.table} "

rule BQRC2:
    """Apply base quality recalibration"""
    input:
        bam=config['output_dir'] + "/splitNcigar/{samp}.rmdup.sort.bam",
        fasta=config['ref_fasta'],
        table=config['output_dir'] + "/BQRC/{samp}_data.table",
    output:
        bam=config['output_dir'] + "/BRQC/{samp}.rmdup.sort.bam",
    shell:
        "{config[gatk]} ApplyBQSR "
        "-R {input.fasta} "
        "-I {input.bam} "
        "--bqsr-recal-file {input.table} "
        "-O {output.bam} "

# ################################# #
# Variants to call  #
# ################################# #    

rule exon_by_gene:
    """ Get exons per gene as a GRangesList from the gft annotation file used for alignment (STAR). Rule adapted from emedlab/snake-pipe"""
    input:
        config['ref_gtf']
    output:
        config['output_dir'] + "/fSNPs/b37_ebg.rds",
        config['output_dir'] + "/fSNPs/gene_coord.txt"
    script:
         "Scripts/exon_by_gene.R"

rule fSNP_coord:
    """Get fSNP coordinates per gene. Rule adapted from emedlab/snake-pipe/analysis"""
    input:
        ebg=config['output_dir'] + "/fSNPs/b37_ebg.rds",
        legend=config['1000G'] + "/1000GP_Phase3_chr{chrom}.legend.gz"
    params:
        maf=0.01,
        population="EUR"
    output:
        fsnps=config['output_dir'] + "/fSNPs/chr.{chrom}.fSNP.RP.genes.txt"
    script:
        "Scripts/fSNP_RP.R"

rule list_vars:
    """Make a list of variant positions to call to speed up variant callers. I selected fSNPs with 0.01 maf cut-off (files prepared in emedlab pipeline, selected SNPs in the 1000G phase 3 overlapping the union of gene exons). File bed format is in the form <chr> <start> <stop> 0-base coordinates. I tried ".list" (GATK format but wasnt recognised. Adapted from RNA_call"""
    input:
        fsnps=config['output_dir'] + "/fSNPs/chr.{chrom}.fSNP.RP.genes.txt"
    output:
        out=config['output_dir'] + "/fSNPS/fSNPs_{chrom}_RP_maf0_01.bed"
    script:
        "Scripts/variants_list.py"


# ############################################
# Prepare files for genotype quality control #
# ############################################

rule SNP_cluster:
    """For the SNPS I want to genotype, given a window and cluster size assess whether each SNP is within a cluster of the indicated size within the window. Adapted from RNA_call/paper"""
    input:
        snps=config['fSNP_dir'] + "/chr.{chrom}.fSNP.RP.genes.txt"
    params:
        window=35,
        cluster_size=3
    output:
        out=config['output_dir'] + "/fSNPS_QC/fSNPs_{chrom}_RP_maf0_01_cluster3window35.txt"
    script:
        "Scripts/count_clusters.py"

rule allelic_counts_fwd_rev:
    """Allelic counts with the addition of whether the read is forward or reverse to perform Fisher test. This is the filter GATK perform for strand bias"""
    input:
        bam=config['output_dir'] + "/unique_reads/allelic_counts/{samp}.rmdup.sort.bam",
    output:
        counts=config['output_dir'] + "/allelic_counts_unique_reads_fwd_rev/{samp}.Q20.alleleCounts.txt"
    params:
        snp_dir=config['fSNP_dir'],
        base_qual=20,
        output_dir=config['output_dir'] + "/allelic_counts_unique_reads_fwd_rev"
    shell:
        "python {config[allele_reads]}/count_intersecting_strand.py "
        " --is_paired_end --is_sorted "
        "--base_qual {params.base_qual} "
        " --output_dir {params.output_dir} "
        " --snp_dir {params.snp_dir} "
        " {input.bam} ; "
        " mv {params.output_dir}/{wildcards.samp}.rmdup.sort.post_remapping_AI.txt {params.output_dir}/{wildcards.samp}.Q20.alleleCounts.txt"

rule fisher_strand:
    """Compute Fisher strand test for SNP QC purposes"""
    input:
        counts = expand(config['output_dir'] + "/allelic_counts_unique_reads_fwd_rev/{samp}.Q20.alleleCounts.txt", samp=d.keys())
    output:
        FS = config['output_dir'] + "/allelic_counts_unique_reads_fwd_rev/FS.Q20.alleleCounts.txt"
    shell:
        "python {config[allele_reads]}/FisherStrand.py "
        " --sample_counts {input.counts} "
        " --output_file {output.FS} "

        
# ################################# #
# Call genotypes with beta binomial  #
# ################################# #


rule uniquely_mapped_reads:
    """Select uniquely mapped reads for calling variants using beta binomial. After GATK pipeline 255 in STAR is now 60"""
    input:
        bam=config['output_dir'] + "/BRQC/{samp}.rmdup.sort.bam",
    output:
        bam=config['output_dir'] + "/unique_reads/allelic_counts/{samp}.rmdup.sort.bam",
    params:
        mapq=60
    shell:
       "samtools view  -b -o {output.bam} -q {params.mapq} -h {input.bam}  ; "
        

rule allelic_counts:
    """For fSNPs look at allelic counts, filtered by mapq and base Q"""
    input:
        bam=config['output_dir'] + "/unique_reads/allelic_counts/{samp}.rmdup.sort.bam",
    output:
        counts=config['output_dir'] + "/allelic_counts_unique_reads/{samp}.Q20.alleleCounts.txt"
    params:
        snp_dir=config['fSNP_dir'],
        base_qual=20,
        output_dir=config['output_dir'] + "/allelic_counts"
    shell:
        "python {config[refbias_dir]}/count_intersecting_QC.py "
        " --is_paired_end --is_sorted "
        "--base_qual {params.base_qual} "
        " --output_dir {params.output_dir} "
        " --snp_dir {params.snp_dir} "
        " {input.bam} ; "
        " mv {params.output_dir}/{wildcards.samp}.rmdup.sort.post_remapping_AI.txt {params.output_dir}/{wildcards.samp}.Q20.alleleCounts.txt"
        
rule split_by_chrom:
    """Split allelic counts by chromosome to then call genotypes"""
    input:
        config['output_dir'] + "/allelic_counts/{samp}.Q20.alleleCounts.txt",
    output:
        config['output_dir'] + "/allelic_counts/{samp}.chr{chrom}.Q20.allelicCounts.txt",
    wildcard_constraints:
        chrom="\d+"
    run:
        df=pd.read_csv(input[0], sep=" ")
        df=df.loc[df['CHROM'] == int(wildcards.chrom)]
        df.to_csv(output[0], sep=" ", index=False)

rule pool_reads:
    """Optional rule. Prepares a file pooling reads from all samples for model training"""
    input:
        counts=expand(config['output_dir'] + "/allelic_counts/{samp}.Q20.alleleCounts.txt", samp=d.keys()),
    params:
        N=1000,
        depth=10
    output:
        out=config['output_dir'] + "/allelic_counts/pool.Q20.alleleCounts.txt"
    script:
        "Scripts/pool_reads.R"
        
rule fit_BB:
    """Use this rule only if you want to train the model  instead of using the default model. Here the model is train with each of the study samples, can replace the input file with the output from pool_reads if wanted to train with a pool of reads. For calling genotypes use call_BB_with_model_fit"""
    input:
        counts=config['output_dir'] + "/allelic_counts/{samp}.Q20.alleleCounts.txt"
    params:
        depth=10,
        out=config['output_dir'] + "/fit_beta_binom",
        prefix="{samp}",
        N=1000
    threads:
        4
    output:
        out=config['output_dir'] + "/fit_beta_binom/{samp}_stan_beta_binom_fit.rds"
    script:
        "Scripts/run_fit_BBmix.R"
        
rule call_genotypes:
    """Call genotypes. If using the model fit (model trained in rule fit_BB)."""
    input:
        beta_b=lambda wildcards: config['output_dir'] + "/fit_beta_binom/{samp}_stan_beta_binom_fit.rds" if wildcards.model_fit == "yes" else [],
        counts=config['output_dir'] + "/allelic_counts/{samp}.chr{chrom}.Q20.allelicCounts.txt",
        legend=config["1000G"] + "/1000GP_Phase3_chr{chrom}.legend.gz",
        FS = config['output_dir'] + "/allelic_counts_fwd_rev/FS.Q20.alleleCounts.txt",
        clusters=config['output_dir'] + "/fSNPS_QC/fSNPs_{chrom}_RP_maf0_01_cluster3window35.txt"
    params:
        depth=10,
        population="EUR"
    wildcard_constraints:
        chrom="\d+"
    output:
        gt=config['output_dir'] + "/beta_binom_gt_fit_{model_fit}/{samp}.chrom{chrom}.gt.txt"
    script:
        "Scripts/beta_binom_gt.R"
    
        
rule exclude_hom_all_samples:
    """Exclude homozygous calls across all samples, enriched in artifacts and not informative."""
    input:
        samp=expand(config['output_dir'] + "/beta_binom_gt_fit_{model_fit}/{samp}.chrom{{chrom}}.gt.txt", samp = d.keys(), model_fit=model)
    output:
        gt=config['output_dir'] + "/beta_binom_gt_all_fit_{model_fit}/chrom{chrom}.gt.txt"
    script:
        "Scripts/ex_no_alt.R"

        

        


      
