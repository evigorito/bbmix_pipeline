library(bbmix)


## Get Stan fit object when not using default in package.


stan_f <- snakemake@input[['beta_b']]
fs <- snakemake@input[['FS']]
clus <- snakemake@input[['clusters']]


if(!exists("stan_f")) stan_f <- NULL

if(!is.null(stan_f)) {
    if(length(stan_f) == 0) stan_f  <- NULL 
}

## Check for QC files

if(!exists("fs")) fs <- NULL

if(!exists("clus")) clus <- NULL
   


call_gt(allele_counts_f=snakemake@input[['counts']],
        depth=snakemake@params[['depth']],
        stan_f = stan_f,
        legend_f= snakemake@input[['legend']],
        pop=snakemake@params[['population']],
        fisher_f = fs ,
        cluster_f = clus,
        out=snakemake@params[['out']])
