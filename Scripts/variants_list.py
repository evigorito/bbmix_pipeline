import pandas as pd
import os



def var_list(file, out):
    """Prepare a list of variants to use in haplotypecaller in bed format so those variants will be called. File format is in the form <chr> <start> <stop> 0-base coordinates.
Parameters:
    file (string): name for fSNPs file for a given chromosome
    out_file (string): full name to output file
"""
    a = pd.read_csv(file, delim_whitespace=True, header=None)
    
    # get chromosome
    chrom = int(os.path.basename(file).split(".")[1])
        
    # create df with required cols
    data = pd.DataFrame({'chrom' : chrom, 'start' : a[0]-1, 'end' : a[0]})      
    data.to_csv(out, sep="\t", header=False, index=False)

var_list(file = snakemake.input['fsnps'], out = snakemake.output['out'])        
        

