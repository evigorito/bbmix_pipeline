""" Combine allelic counts for forward and reverse strand across samples. Apply Fisher test to assess strand bias"""

import argparse
import sys
import math
import pandas as pd
import numpy as np
from functools import reduce
from scipy.stats import fisher_exact
from fisher import pvalue_npy



def parse_options():

    parser = argparse.ArgumentParser(description="Fisher test for strand bias")

    parser.add_argument("--sample_counts", "-i",  nargs='+', default=[],
                        help="Full name for files with allelic counts per sample")
    parser.add_argument("--output_file", "-o", action='store',
                        help="Full name for output file")
    options = parser.parse_args()

    return options


def count_reads_snp(files):
    """Avoid memory usege by reading each file at a time"""
    comb = None
    for f in files:
        df = pd.read_csv(f, sep = " ")
        df.set_index(["CHROM", "POS", "REF", "ALT"], inplace = True)
        if comb is None:
            comb = df.copy()
        else:
            comb = reduce(lambda x, y: x.add(y), [comb, df])

    return comb


# def fisher_test(x1,x2,y1,y2):
#     """ Computes Fisher test for strand bias in allelic counts. Returns Phered Score to compare with GATK (-log10(p)*10)"""
#     if y1 == y2 == 0 | x1 == x2 == 0:
#         return float('NaN')
#     else:
#         table=np.array([[x1,x2], [y1,y2]])

#         try:
#             p=-math.log10(fisher_exact(table, alternative='two-sided')[1])*10
#         except:
#             p=float('NaN')
            
#         return p

def fisher_test(df):
     """ Computes Fisher test for strand bias in allelic counts. Returns Phered Score to compare with GATK (-log10(p)*10). Much faster version than before"""
     
     c = df[['NREF_FWD','NREF_REV','NALT_FWD','NALT_REV']].to_numpy(dtype='uint64')
     _, _, twosided = pvalue_npy(c[:, 0], c[:, 1], c[:, 2], c[:, 3])

     df['FS']=-np.log10(twosided)*10

     return df
 

def main(counts, output):

    df = count_reads_snp(counts)
    
    df = fisher_test(df)

    df.to_csv(output, sep=" ", index=True, na_rep="NA")


if __name__ == '__main__':

    #sys.stderr.write("command line: %s\n" % " ".join(sys.argv))
    options = parse_options()

    main(options.sample_counts,
         options.output_file)
    
