import pandas as pd
import numpy as np 



def count_clusters(filename, window, n, outfile):
    """count SNP clusters of size n (excluding lead SNP) whithin a window"""
    df = pd.read_csv(filename, delim_whitespace=True, header=None, names=["POS", "REF", "ALT"])
    pos = df['POS'].to_numpy(dtype=np.int32)

    c = np.zeros(shape = len(pos), dtype = bool)
    for p in pos:
        sub = pos[(pos >= p - window) & (pos <= p + window)]
        # check if p can be in a cluster
        if sub.size > n:
            sub2 = pos[(pos >= p - window) & (pos <= p)]
            # select SNPs upstream of p up to p and check if there is a cluster within window
            for s in sub2:
                if sub[(sub >= s) & (sub <= s + window)].size > n:
                    # when I find a cluster change to True and move on to next SNP
                    c[np.where(pos == p)] = True
                    break

    df["SNPcluster"] = c

    df.to_csv(outfile, sep=" ", index=False)
        
        
count_clusters(filename = snakemake.input['snps'], window = snakemake.params['window'], n = snakemake.params['cluster_size'], outfile =  snakemake.output['out'])  
